# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util, searchAgents

from game import Agent

class ReflexAgent(Agent):
    """
      A reflex agent chooses an action at each choice point by examining
      its alternatives via a state evaluation function.

      The code below is provided as a guide.  You are welcome to change
      it in any way you see fit, so long as you don't touch our method
      headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {North, South, West, East, Stop}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
        minDist = 0

        if (len(newFood.asList())) == 0:
            returnVal = 1
        else:
            returnVal = float(1)/float(len(newFood.asList()))
        if newPos in currentGameState.getFood().asList():
            returnVal += 1
        else:
            if newFood.asList():
                returnVal += 1.0/min([util.manhattanDistance(elem, newPos) for elem in newFood.asList()])
        timesVar = 0
        if sum(newScaredTimes) > 0:
            returnVal += 1
        if sum(newScaredTimes) == 0:
            timesVar = min([util.manhattanDistance(elem.getPosition(), newPos) for elem in newGhostStates])
            if (timesVar > 4):
                timesVar = 0
            if timesVar != 0:
                returnVal -= float(timesVar)
        return returnVal

def scoreEvaluationFunction(currentGameState):
    """
      This default evaluation function just returns the score of the state.
      The score is the same one displayed in the Pacman GUI.

      This evaluation function is meant for use with adversarial search agents
      (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
      This class provides some common elements to all of your
      multi-agent searchers.  Any methods defined here will be available
      to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

      You *do not* need to make any changes here, but you can if you want to
      add functionality to all your adversarial search agents.  Please do not
      remove anything, however.

      Note: this is an abstract class: one that should not be instantiated.  It's
      only partially specified, and designed to be extended.  Agent (game.py)
      is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
      Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action from the current gameState using self.depth
          and self.evaluationFunction.

          Here are some method calls that might be useful when implementing minimax.

          gameState.getLegalActions(agentIndex):
            Returns a list of legal actions for an agent
            agentIndex=0 means Pacman, ghosts are >= 1

          gameState.generateSuccessor(agentIndex, action):
            Returns the successor game state after an agent takes an action

          gameState.getNumAgents():
            Returns the total number of agents in the game

          gameState.isWin():
            Returns whether or not the game state is a winning state

          gameState.isLose():
            Returns whether or not the game state is a losing state
        """
        def treeRekurshun(self, gameState, agent007, depth):
            agent007 = agent007 % gameState.getNumAgents()
            if not agent007:
                depth = depth - 1
            if depth == 0 or gameState.isWin() or gameState.isLose():
                return self.evaluationFunction(gameState)
            values = []
            for action in gameState.getLegalActions(agent007):
                values.append((action, treeRekurshun(self, gameState.generateSuccessor(agent007, action), agent007 + 1, depth)))
            if values:
                if not agent007:
                    if self.depth == depth:
                        return max(values ,key=lambda x:x[1])[0]
                    return max(values ,key=lambda x:x[1])[1]
                else:
                    return min(values ,key=lambda x:x[1])[1]

        return treeRekurshun(self, gameState, 0, self.depth + 1)

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
      Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
          Returns the minimax action using self.depth and self.evaluationFunction
        """
        def treeRekurshun(self, gameState, agent007, depth, alpha, beta):

            def maxValue(state, alpha, beta):
                v = -float('inf')
                nextAction = ""
                for action in state.getLegalActions(agent007):
                    nextValue = treeRekurshun(self, gameState.generateSuccessor(agent007, action), agent007 + 1, depth, alpha, beta)
                    if v < nextValue:
                        v = nextValue
                        nextAction = action
                    if v > beta:
                        return (nextAction, v)
                    alpha = max(alpha, v)
                return (nextAction, v)

            def minValue(state, alpha, beta):
                v = float('inf')
                for action in state.getLegalActions(agent007):
                    nextAction = ""
                    nextValue = treeRekurshun(self, gameState.generateSuccessor(agent007, action), agent007 + 1, depth, alpha, beta)
                    if v > nextValue:
                        v = nextValue
                        nextAction = action
                    if v < alpha:
                        return (nextAction, v)
                    beta = min(beta, v)
                return (nextAction, v)

            agent007 = agent007 % gameState.getNumAgents()
            if not agent007:
                depth = depth - 1
            if depth == 0 or gameState.isWin() or gameState.isLose():
                return self.evaluationFunction(gameState)

            # if current state is for pacman
            if not agent007:
                if self.depth == depth:
                    # return an ACTION here
                    return maxValue(gameState, alpha, beta)[0]
                #return a value here
                return maxValue(gameState, alpha, beta)[1]
                # if current state is for a ghost
            else:
                #return a value here
                return minValue(gameState, alpha, beta)[1]

        return treeRekurshun(self, gameState, 0, self.depth + 1, -float('inf'), float('inf'))

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
          Returns the expectimax action using self.depth and self.evaluationFunction

          All ghosts should be modeled as choosing uniformly at random from their
          legal moves.
        """
        "*** YOUR CODE HERE ***"
        def treeRekurshun(self, gameState, agent007, depth):
            agent007 = agent007 % gameState.getNumAgents()
            if not agent007:
                depth = depth - 1
            if depth == 0 or gameState.isWin() or gameState.isLose():
                return self.evaluationFunction(gameState)
            values = []
            for action in gameState.getLegalActions(agent007):
                values.append((action, treeRekurshun(self, gameState.generateSuccessor(agent007, action), agent007 + 1, depth)))
            if values:
                if not agent007:
                    if self.depth == depth:
                        return max(values ,key=lambda x:x[1])[0]
                    return max(values ,key=lambda x:x[1])[1]
                else:
                    average = 0
                    for value in values:
                        if not value[1]:
                            average += 0
                        else:
                            average += value[1]
                    average = float(average) / float(len(values))
                    return average

        return treeRekurshun(self, gameState, 0, self.depth + 1)


def betterEvaluationFunction(currentGameState):
    """
      Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
      evaluation function (question 5).

      DESCRIPTION: <write something here so we know what you did>
    """
    currPos = currentGameState.getPacmanPosition()
    food = currentGameState.getFood()
    ghostStates = currentGameState.getGhostStates()
    scaredTimes = [ghostState.scaredTimer for ghostState in ghostStates]
    if currentGameState.isWin():
        return float('inf')
    if currentGameState.isLose():
        return -float('inf')

    numFoodLeftReciprikal =  1/float(len(food.asList()))



    distanceToFood = 1.0/min([util.manhattanDistance(elem, currPos) for elem in food.asList()])
    chaseGhosts = sum(scaredTimes) > 0
    capsoolDist = 0
    ghostDistance = 0
    timesVar = min([util.manhattanDistance(elem.getPosition(), currPos) for elem in ghostStates])
    if not chaseGhosts:


        if (timesVar > 4):
            ghostDistance = 0
        else:
            ghostDistance = float(timesVar)/4.0
        if currentGameState.getCapsules():
            capsoolDist = 1.0/min([manhattanDistance(elem, currPos) for elem in currentGameState.getCapsules()])
    ruHigh = 0
    if chaseGhosts:
        ruHigh = 15
        ghostDistance = -timesVar

       # print currentGameState.getCapsules()
    #print "numFoodLeftReciprikal: ", numFoodLeftReciprikal, " distanceToFood: ",  distanceToFood, " capsoolDist: ", 10.0 * capsoolDist, " ghostDistance: ", ghostDistance/4.0, " ruHigh: ", ruHigh, " score: ", currentGameState.getScore()/10.0
    return numFoodLeftReciprikal + distanceToFood + 10.0 * capsoolDist + ghostDistance/4.0 + random.random()/1000.0 + ruHigh + currentGameState.getScore()/10.0





    #
    # returnVal = 0
    # foodList = currentGameState.getFood().asList()
    # pacmanPosition = currentGameState.getPacmanPosition()
    # ghosts = currentGameState.getGhostStates()
    # distanceToClosestPellet = float(1.0)/float(min([manhattanDistance(elem, pacmanPosition) for elem in foodList]))
    # numberOfPelletsLeft = float(currentGameState.getFood().count(True))
    # # distanceToClosestGhost = min([searchAgents.mazeDistance(elem, pacmanPosition, currentGameState) for elem in ghosts])
    # distanceToClosestGhost = min([manhattanDistance(elem.getPosition(), pacmanPosition) for elem in ghosts])
    # distanceToClosestCapsule = min([manhattanDistance(elem, pacmanPosition) for elem in currentGameState.getCapsules()])
    # scaredTimes = [ghostState.scaredTimer for ghostState in ghosts]
    # print scaredTimes
    # if len(scaredTimes) > 1:
    #     distanceToClosestGhost = -distanceToClosestGhost

    # print ("pellets:", float(1)/((float(numberOfPelletsLeft)/float(len(foodList)))))
    # print ("ghost dist:", (float(distanceToClosestGhost)/float(4)))
    # print ("dist pellet:", distanceToClosestPellet)
    #print ("ghost:", float(distanceToClosestGhost))
    # print ("food:", (float(numberOfPelletsLeft)/float(len(foodList)))*float(distanceToClosestPellet))
    # print(float(1)/float(distanceToClosestCapsule)) - float(len(currentGameState.getCapsules()))
    # print (float(distanceToClosestGhost)/float(4)) + (float(numberOfPelletsLeft)/float(len(foodList)))*float(distanceToClosestPellet)
    # return (float(distanceToClosestGhost)) + numberOfPelletsLeft
    # return -(float(distanceToClosestGhost)/float(6)) + (float(numberOfPelletsLeft)/float(len(foodList)))*float(distanceToClosestPellet)
    #return 2*((float(1)/float(distanceToClosestCapsule)) - float(len(currentGameState.getCapsules()))) + (float(distanceToClosestGhost)/float(8))


# if (len(newFood.asList())) == 0:
#     returnVal = 1
# else:
#     returnVal = float(1) / float(len(newFood.asList()))
# if newPos in currentGameState.getFood().asList():
#     returnVal += 1
# else:
#     if newFood.asList():
#         returnVal += 1.0 / min([util.manhattanDistance(elem, newPos) for elem in newFood.asList()])
# timesVar = 0
# if sum(newScaredTimes) > 0:
#     returnVal += 1
# if sum(newScaredTimes) == 0:
#     timesVar = min([util.manhattanDistance(elem.getPosition(), newPos) for elem in newGhostStates])
#     if (timesVar > 4):
#         timesVar = 0
#     if timesVar != 0:
#         returnVal -= float(timesVar)
# return returnVal
# Abbreviation
better = betterEvaluationFunction
